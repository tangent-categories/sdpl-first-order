# SDPL-First-Order

## Build instructions.

Requirements:
  * Haskell stack
  * llvm-12: in particular, llc, opt, llvm-dis, llvm-link, polly, openmp, lld, and clang should be installed from the version 12 branch of the llvm project.  Follow the instructions to build llvm-12 tools below, or install from a package manager.  On Ubuntu you can run 
      
      sudo apt search llvm | grep 12

  to find necessary programs.  On Arch you can run 
      
      sudo pacman -Ss llvm | grep 12 

  At the time of this writing llvm-12 is in the extra/ repository whereas llvm-13 is in Aur.  Unlike Ubuntu Arch doesn't keep around all the llvm versions.
  Eventually, we should create a real build script that pulls and builds the correct llvm version.

Currently, there is a bug in the build.sh file on the first run ever.  If you run it, it will install bnfc into ~/.local.  However, this installation won't be detected
until the shell is restarted.

TODO: How do you detect the installed bnfc without restarting the shell? 

Building:

Run the build.sh script.  If it's the first time ever being run, do 

    chmod +x build.sh

to enable the script to be run.  Then do

    ./build.sh

This will pull bnfc, install it, run bnfc on the Language.cf file, generate all the syntax processing files, and do a happy analysis on the grammar to detect ambiguities 
and errors.  The grammar info will be stored in grammar-ambigs.txt.  If there are any errors they will be in the top of the file.  Note: we compile the Language.cf file 
with the options --text-token and --functor.  The --functor option stores the source location of every symbol in the parse tree.  The --text-token uses Data.Text instead 
of String -- this is recommended due to subtle bugs in reading files as String. 

Rebuilding:

On rebuilds, only stack build needs to be run.  If using VSCode with the official Haskell plugin, it needs to be run initially from the commandline, and subsequent builds 
can be performed in the editor.  In general, to rebuild the program just run 

    stack build

The build.sh file only needs to be run in case the Language.cf file was changed.

TODO: change the project name?

Running:

Use 

    stack run