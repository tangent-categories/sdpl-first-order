{-# LANGUAGE FlexibleContexts #-}

module Main where

-- Our imports
import Lang.TypeCheck (typecheck,prettyPrintTypeFull,typecheckFunctionFull,prettyPrintFunType)
import SDPLMonad
import SimpleInterpreter (weakHeadNormalizeWithEnv)
import TermUtils (freeVarsFunction,getFunNameBody)

-- Autogenerated imports
import Lang.Lex (mkPosToken,prToken,Token(..))
import Lang.Par (myLexer, pTerm,pFunDef)
import Lang.Print (Print,printTree)
import Lang.Abs (Term (..),FunDef (..), Type (..))

-- System imports
import Data.Text (Text) -- import only the name Text from Text
import qualified Data.Text as Text (pack,unpack)
import System.Console.Haskeline

-- Control imports 
import Data.Bifunctor (bimap)
import Control.Applicative
import Control.Monad.Except 
import Control.Monad.State.Strict 
import qualified Data.Map as M
import Control.Monad.IO.Class (MonadIO)


--type FunCont = [(String,([type], type))] 
--do the same thing for the variable context
--split up phi ang gamma
--gamma is a list of lists, phi isn't

main :: IO ()
main = driveLoop
    -- s <- getLine
    -- print( pTerm(myLexer s))
    -- case pTerm(myLexer s) of 
    --    Left message -> putStrLn("error!!"++message)
    --    Right term -> case (typecheck [[("x",UnitType)]] [] UnitType term) of  
    --                   False -> putStrLn("type failed")
    --                   True -> putStrLn("well typed")

    -- putStrLn"hi"


showTree :: 
    (Control.Monad.IO.Class.MonadIO m, Show a,Print a) 
    => a 
    -> InputT m ()
showTree tree = do 
    outputStrLn $ "\n[Parse Tree]\n\n" ++ show tree
    outputStrLn $ "\n[Linearized Tree]\n\n" ++ printTree tree

-- ((l,c),s) is line/row and column from the token in string form
showPosToken  :: 
    (Show a,Show b,Show c)
    => ((a,b),c) -> String
showPosToken ((l,c),s) = concat [show l, ":", show c,"\t",show s]

-- In the rest of the file we define combinators for compiling from input.

-- At the top level, either we're compiling a term or a function definition
-- The syntax is non-overlapping -- that is, both can't succeed, we'll try compiling a 
-- term first, and if that fails, compile as a function definition.
-- ts is a list of tokens
tryCompileStore ts = compileTerm ts <|> compileFunction ts

compileTerm :: 
    [Token]
    -> SDPLMonadT String (InputT (StateT (M.Map Text ([Type],Type),M.Map Text Term) IO )) ()
compileTerm ts = do 
    -- attempt parse
    term <- tryParseTerm ts 
    (typeEnv,valEnv) <- lift $ lift get
    let typeEnvList = map (\(name,(in_tys,out_ty)) -> (Text.unpack name,(in_tys,out_ty))) $ M.toList typeEnv
    -- attempt typecheck
    ty_term <- tryTypeCheck (typecheck [] typeEnvList term)
    lift $ outputStrLn $ "\n[Type]\n    " ++ prettyPrintTypeFull ty_term
    -- attempt evaluation
    let evald = weakHeadNormalizeWithEnv term valEnv
    lift $ outputStrLn $ "\n[Evaluation]\n    " ++ show evald

    
compileFunction ::
    [Token]
    -> SDPLMonadT String (InputT (StateT (M.Map Text ([Type],Type),M.Map Text Term) IO )) ()
compileFunction ts = do 
    funDef <- tryParseFunction ts
    lift $ outputStrLn $ "[Linearized Tree]\n    " ++ printTree funDef
    typeEnvRaw <- lift $ lift $ gets fst
    let typeEnv = M.intersection typeEnvRaw (freeVarsFunction funDef)
    let typeEnvList = map (\(name,(in_tys,out_ty)) -> (Text.unpack name,(in_tys,out_ty))) $ M.toList typeEnv
    ty_FunDef <- tryTypeFun (typecheckFunctionFull typeEnvList funDef)
    lift $ outputStrLn $ "\n[Type]\n    " ++ prettyPrintFunType ty_FunDef
    let (name,body) = getFunNameBody funDef
    lift $ lift $ modify $ bimap (M.insert name ty_FunDef) (M.insert name body)
 
tryParseTerm :: 
    (Monad m)
    => [Token]
    -> SDPLMonadT String m Term
tryParseTerm ts = case pTerm ts of 
    Left err -> throwError $ "\nParse failed...\n" ++ "[Tokens]\n" ++ concatMap ((++ "\n") . showPosToken . mkPosToken) ts ++ err ++ "\n"
    Right termTree -> return termTree 

tryParseFunction :: 
    (Monad m)
    => [Token]
    -> SDPLMonadT String m FunDef   
tryParseFunction ts = case pFunDef ts of 
    Left err -> throwError $ "\nParse failed...\n" ++ "[Tokens]\n" ++ concatMap ((++ "\n") . showPosToken . mkPosToken) ts ++ err ++ "\n"
    Right termTree -> return termTree 

tryTypeCheck :: 
    (Monad m,MonadIO m) 
    => Either String Type 
    -> SDPLMonadT String (InputT m) Type
tryTypeCheck tyEither = case tyEither of 
    Left msg -> do 
        lift $ outputStrLn $ "[Type error]\n    " ++ show msg 
        throwError $ "[Type error]\n    " ++ show msg 
    Right ty -> return ty
tryTypeFun :: 
    (Monad m,MonadIO m)
    => Either String ([Type],Type)
    -> SDPLMonadT String (InputT m) ([Type],Type)
tryTypeFun tysEither = case tysEither of 
    Left msg -> do 
        lift $ outputStrLn $ "[Type error]\n    " ++ show msg 
        throwError $ "[Type error]\n    " ++ show msg 
    Right tys -> return tys


driveLoop = do 
    res <- evalStateT (runInputT defaultSettings $ runExceptT loop) (M.empty,M.empty)
    case res of 
        Left s -> driveLoop -- revisit this... in the case of a failure that propagates this high, we reset the repl...
        Right x0 -> putStrLn "finished"

loop = do 
    lift $ outputStrLn  "\nNext term:"
    minput <- lift $ getInputLine "> "
    case minput of 
        Nothing -> loop
        Just ":q" -> lift $ return ()
        Just "quit" -> lift $ return ()
        Just "exit" -> lift $ return ()
        Just ":m" -> do 
            ls <- getAllLines 
            let lsConc = concat ls 
            tryCompileStore (myLexer (Text.pack lsConc))
            loop 
        Just ":M" -> do 
            ls <- getAllLines 
            let lsConc = unlines ls 
            tryCompileStore $ myLexer  $ Text.pack lsConc
            loop
        Just inputString -> do 
            tryCompileStore $ myLexer  $ Text.pack inputString 
            loop

getAllLines = do 
    minput <- lift $ getInputLine "$:> "
    case minput of 
        Nothing -> return[]
        Just ":end" -> return []
        Just l -> do 
            ls <- getAllLines 
            return (l:ls)
