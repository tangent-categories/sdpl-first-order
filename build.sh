#!/bin/bash

# In order to compile the program, we require additional tools that aren't packaged as part of stack.
# One tool is BNFC.  While BNFC is a Haskell program, the build tool stack doesn't have an option to 
# run BNFC as a precompile step.  Placing the BNFC build, and grammar ambiguity check into a script is the simplest option.
# Another tools is llvm.  Due to some the idiosyncracies of building llvm on Windows, we require that a developer 
# take care to ensure that the llvm projects: llc, opt, llvm-link, lld, llvm-dis, polly, runtime-rt, and clang are installed.
# In fact, until we have time to write a proper build script, we recommend building this through WSL2 on Windows.


# check if the bnfc program is up to date 
BNFC_VERSION="$(bnfc --version)"
echo $BNFC_VERSION
echo $BNFC_VERSION
BNFC_REQUIRED="2.9.3"
echo $BNFC_REQUIRED
# The following checks if the bnfc is the correct version
if [ "$(printf '%s\n' "$BNFC_REQUIRED" "$BNFC_VERSION" | sort -V | head -n1)" = "$BNFC_REQUIRED" ]; then 
    echo "Recent enough BNFC version found"
else 
    echo "BNFC version too old, building newer version"
    cd ..
    git clone https://github.com/BNFC/bnfc.git
    cd bnfc/source 
    stack install --stack-yaml stack-9.0.1.yaml
   
fi

echo "Beginning language syntax generation"
# Rebuild the cf file with the correct options and then build the project
cd src
bnfc --haskell -d --text-token --functor Lang.cf
# go into the Language folder
cd Lang
# TODO: do a similar check to see if happy is installed
happy --info=grammar-ambigs.txt --outfile=grammar.out Par.y
# remove the grammar.out file; it's not needed
rm grammar.out
# also remove Par.hs; it's not needed 
# rm Par.hs
# leave the directory
cd ..
# we're now back in the src directory
# let's move the grammar-ambigs to the project root 
mv Lang/grammar-ambigs.txt ../grammar-ambigs.txt

# Remove the unused file that also breaks the build
rm Lang/Test.hs

# Insert similar stuff for building llvm, grin, z3, etc -- i.e. non-haskell tools needed.  Eventually 
# figure out how to build them without registering and pass the arguments in as needed

# Go to the prjoect root and build the project
cd ..
stack build

echo "build complete"
