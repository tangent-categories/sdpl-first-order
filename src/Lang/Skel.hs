-- File generated by the BNF Converter (bnfc 2.9.3).

-- Templates for pattern matching on abstract syntax

{-# OPTIONS_GHC -fno-warn-unused-matches #-}

module Lang.Skel where

import Prelude (($), Either(..), String, (++), Show, show)
import qualified Lang.Abs

type Err = Either String
type Result = Err String

failure :: Show a => a -> Result
failure x = Left $ "Undefined case: " ++ show x

transPIdent :: Lang.Abs.PIdent -> Result
transPIdent x = case x of
  Lang.Abs.PIdent string -> failure x

transNumber :: Lang.Abs.Number -> Result
transNumber x = case x of
  Lang.Abs.Number string -> failure x

transLMod :: Show a => Lang.Abs.LMod' a -> Result
transLMod x = case x of
  Lang.Abs.LModule _ fundefs -> failure x

transFunDef :: Show a => Lang.Abs.FunDef' a -> Result
transFunDef x = case x of
  Lang.Abs.Function _ pident tyvars type_ term -> failure x

transType :: Show a => Lang.Abs.Type' a -> Result
transType x = case x of
  Lang.Abs.UnitType _ -> failure x
  Lang.Abs.RealType _ -> failure x
  Lang.Abs.TupleType _ type_1 type_2 -> failure x

transTyvar :: Show a => Lang.Abs.Tyvar' a -> Result
transTyvar x = case x of
  Lang.Abs.TypedVariable _ pident type_ -> failure x

transTerm :: Show a => Lang.Abs.Term' a -> Result
transTerm x = case x of
  Lang.Abs.Sub _ tyvar term1 term2 -> failure x
  Lang.Abs.Var _ pident -> failure x
  Lang.Abs.RealTerm _ number -> failure x
  Lang.Abs.NullTerm _ -> failure x
  Lang.Abs.AddTerms _ term1 term2 -> failure x
  Lang.Abs.UnitTerm _ -> failure x
  Lang.Abs.TupleTerm _ term1 term2 -> failure x
  Lang.Abs.FirstTerm _ term -> failure x
  Lang.Abs.SecondTerm _ term -> failure x
  Lang.Abs.WhenTerm _ bthens -> failure x
  Lang.Abs.DerivativeTerm _ term tyvars terms1 terms2 -> failure x
  Lang.Abs.BlockTerm _ stmts term -> failure x
  Lang.Abs.MultFunctionTerm _ term1 term2 -> failure x
  Lang.Abs.SinFunctionTerm _ term -> failure x
  Lang.Abs.CosFunctionTerm _ term -> failure x

transBThen :: Show a => Lang.Abs.BThen' a -> Result
transBThen x = case x of
  Lang.Abs.BT _ b term -> failure x

transStmt :: Show a => Lang.Abs.Stmt' a -> Result
transStmt x = case x of
  Lang.Abs.SVar _ tyvar term -> failure x
  Lang.Abs.SDo _ stmts b -> failure x

transB :: Show a => Lang.Abs.B' a -> Result
transB x = case x of
  Lang.Abs.LessThan _ term1 term2 -> failure x
  Lang.Abs.TrueBool _ -> failure x
  Lang.Abs.FalseBool _ -> failure x
  Lang.Abs.OrBool _ b1 b2 -> failure x
  Lang.Abs.AndBool _ b1 b2 -> failure x
