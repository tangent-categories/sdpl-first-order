
module Lang.TypeCheck where

import Lang.Abs
import Data.Typeable
import Data.Text (Text)
import qualified Data.Text as Text
import Control.Monad hiding (zipWithM)

type VarCont = [(String, Type)]
type FunCont = [(String,([Type], Type))]

type TypingEnvironment = [VarCont]


todo :: a
todo = error "TODO: Not yet implemented"


varLookup :: VarCont -> String -> Maybe Type
varLookup [] v = Nothing
varLookup ((name,ty):cs) v
  | name == v =  Just ty
  | otherwise =  varLookup cs v

varLookupEnv :: TypingEnvironment -> String -> Maybe Type
varLookupEnv [] _ = Nothing
varLookupEnv (te:tes) x = case varLookup te x of
  Nothing -> varLookupEnv tes x
  Just ty -> Just ty


getType::Tyvar -> Type
getType (TypedVariable _ _ t) = t

getName::Tyvar -> String
getName (TypedVariable _ (PIdent (_, name)) _) = Text.unpack name


addVarToCurrentContext:: TypingEnvironment->(String, Type)->TypingEnvironment -- this'll add the desired variable to the first context in the list
addVarToCurrentContext [] _ = []
addVarToCurrentContext (c:cs) v = (v:c):cs



checkBool:: TypingEnvironment-> FunCont->B -> Bool
checkBool te fc m = todo
--  case m of
--   OrBool t1 t2 -> checkBool te fc t1 && checkBool te fc t2
--   AndBool t1 t2 -> checkBool te fc t1 && checkBool te fc t2
--   LessThan t1 t2 -> if (typecheck te fc RealType t1 && typecheck te fc RealType t2)  then True else False
--   TrueBool -> True
--   FalseBool -> True


--what does this do?
--it takes a list of BThens -- bools and terms -- and checks if 
--1. the bools are all good
--2.the terms are all of the required type
bThenThing:: TypingEnvironment->FunCont-> Type -> [BThen] -> Bool
bThenThing te fc ty [] = True
bThenThing te fc ty ((BT _ b m):ls) = todo
-- checkBool te fc b && typecheck te fc ty m && bThenThing te fc ty ls -- do you need checkBool or is that taken care of by the parser?


addTypedVarsToCont:: [Tyvar] -> [(String, Type)]-> [(String, Type)]
addTypedVarsToCont [] vs = vs
addTypedVarsToCont ((TypedVariable _ (PIdent (_, name)) ty):as) vs = addTypedVarsToCont as ((Text.unpack name, ty):vs)--is the order these are added in a problem?

addTypedVarsToEnv:: [Tyvar] -> TypingEnvironment-> TypingEnvironment
addTypedVarsToEnv tvs env = makeContext tvs : env

makeContext :: [Tyvar] -> VarCont
makeContext  = map (\(TypedVariable _ (PIdent ((_, _), x)) ty) -> (Text.unpack x,ty))



newContFinder:: VarCont -> Stmt -> Maybe VarCont
newContFinder cont s = todo
--  case s of
--   SVar tv m -> if(typecheck [cont] [] (getType tv) m) then Just ((getName tv, getType tv):cont) else Nothing
--   SDo l b -> case goThroughListOfStmts cont l of
--    Just c' -> if checkBool [c'] [] b then Just cont else Nothing
--    Nothing -> Nothing





goThroughListOfStmts:: VarCont -> [Stmt] -> Maybe VarCont
goThroughListOfStmts cont [] = Just cont
goThroughListOfStmts cont (s:rest) =
 case newContFinder cont s  of
  Just c'-> goThroughListOfStmts c' rest
  Nothing -> Nothing
  

zipWithM :: (a -> b -> Maybe c) -> [a] -> [b] -> Maybe [c]
zipWithM _ [] [] = Just []
zipWithM _ [] _ = Nothing
zipWithM _ _ [] = Nothing
zipWithM f (x:xs) (y:ys) = do
  c <- f x y
  cs <- zipWithM f xs ys
  return (c:cs)


prettyPrintTypeFull :: Type -> String
prettyPrintTypeFull = show -- make this better
prettyPrintFunType :: ([Type],Type) -> String 
prettyPrintFunType = show

typecheckFunctionFull :: FunCont -> FunDef -> Either String ([Type],Type)
typecheckFunctionFull = undefined 

typecheck:: TypingEnvironment -> FunCont -> Term -> Either String Type
typecheck te fc m  = todo
--  case m of
--   Var (PIdent (_, name)) -> varLookupEnv te name
--   NullTerm -> return UnitType
--   RealTerm d -> case ty == RealType of
--           True -> return RealType -- use real haskell code stylke
--           False -> Nothing
--     -- guard (ty == RealType) 
--         -- -- return RealType 
--         -- return RealType
--   AddTerms n p ->  if ((typecheck te fc ty n) && (typecheck te fc ty p)) then True else False
--   Sub tv n p -> if((typecheck te fc (getType tv) n)&&(typecheck (addVarToCurrentContext te (getName tv, getType tv)) fc ty p )) then True else False
--   UnitTerm -> if ty == UnitType then True else False
--   TupleTerm n p ->
--    case ty of
--     TupleType a b -> if ((typecheck te fc a n) && (typecheck te fc b p)) then True else False
--     otherwise -> False
--   FirstTerm n ->
--    case n of
--     TupleTerm a b -> if(typecheck te fc ty a) then True else False
--     otherwise -> False
--   SecondTerm n ->
--    case n of
--     TupleTerm b a -> if(typecheck te fc ty a) then True else False
--     otherwise -> False
--   WhenTerm l -> if(bThenThing te fc ty l) then True else False -- tested a bit
--   DerivativeTerm n tvs ts1 ts2 -> do
--           tym <- typecheck (addTypedVarsToEnv tvs te) fc ty m
--           -- mapM :: (a -> Maybe b) -> ([a]) -> Maybe [b] -- mapM f xs applies f to each x in xs, and if any fails returns Nothing otherwise, returns Just (map f xs)
--                 -- zipWithM :: Applicative m => (a -> b -> m c) -> [a] -> [b] -> m [c]           
--                 -- map h (zip xs ys)  -- zipWith h xs ys
--           zipWithM (\tvar varg -> typecheck te fc  (getType tvar) varg ) tvs ts1
--           zipWithM (\tvar varg -> typecheck te fc  (getType tvar) varg ) tvs ts2
--           return tym
--         -- if  (typecheck (addTypedVarsToEnv tvs te) fc ty m 
--         --     &&(and (map (\x -> typecheck te fc (fst x) (snd x)) (zip (map (\x -> getType  x) tvs) ts1))) 
--         -- 	&&(and (map (\x -> typecheck te fc (fst x) (snd x)) (zip (map getType tvs) ts2)))) 
--         --   then True 
--         --   else False -- untested. I should also make it check the lists are all of the same size (and bigger than 0?) Also, for some reason this it stalls when I input a derivative term with this line...
--   BlockTerm l n -> case goThroughListOfStmts (head te)  l of -- untested
--    Just c'' -> typecheck (c'':te) fc ty n
--    Nothing -> False
--   otherwise -> False



  --let y:R = 3 in (3)

{-
lookup::Context -> Variable -> Bool
	lookup [] v = Falses
	lookup  c:cs v = if (c == v) then (true) else (false)


getType::Tyvar -> Type
getName::Tyvar -> String

addvartocurrentcontext::TypingEnvironment->variable->TypingEnvironment -- this'll add the desired variable to the first context in the list

checkBool:: TypingEnvironment-> B -> bool
checkBool te m =
case m of
OrBool t1 t2 -> checkBool t1 && checkBool t2
AndBool t1 t2 -> checkBool t1 && checkBool t2
LessThan t1 t2 -> if typeCheck te RealType t1 && typeCheck te RealType t2  then True else False
TrueBool -> True
FalseBool -> False


we need a function that folds over the list of bthens and checks if the bools are good and if
the terms have the right type

bThenThing:: TypingEnvironment->type -> [bthen] -> bool
bThenThing te ty [] = true
bThenThing te ty (BT b m):ls = checkBool te b && typeCheck te ty m && bThenThing te ty ls


addTypedVarsToCont:: [Tyvar] -> [Variable]-> [Variable]
addTypedVarsToCont [] vs = vs
addTypedVarsToCont (TypedVariable name ty):as vs = addTypedVars as (name, ty):vs
	
addTypedVarsToEnv:: [Tyvar] -> TypingEnvironment-> TypingEnvironment-
addTypedVarsToEnv tvs [] = [addTypedVarsToCont tvs []]
addTypedVarsToEnv tvs a:as = (addTypedVarsToCont tvs a):as

--fix up the list of lists deal
typecheck:: TypingEnvironment-> type -> term -> bool
typeCheck te ty m  =
	case m of
 		Var name -> if(lookup (name,ty) cont) then (true) else (false)
		NullTerm -> true
		RealTerm d -> do
			t <- :type d
			if (t == double && ty ==RealType) then return true else return false
		AddTerms m n -> do
			b1 <- typeCheck cont ty m
			b2 <- typeCheck cont ty n
			if (b1 == b2 && b2 == true) then return true else return true\
		FunctionCallTerm name args -> hardcode these in

		

		Sub tv m n -> do
			b1 <- typeCheck te (getType tv) m
			b2 <- typeCheck (addvartocurrentcontext te tv) ty n -- need to work out how tyvar workis with this deal -- syntax isn't correct!
			if(b1&&b2) return true else return false
		  UnitTerm -> if ty == UnitType then true else false
		 TupleTerm m n -> do
			case ty of 
				TupleType a b -> do
					b1 <- typeCheck te a m
					b2 <- typeCheck te b n
					if (b1 &b2) then return true else false
				UnitType -> return false
				realType -> return false
		FirstTerm m -> do
			case m of
				TupleTerm a b -> do
					b1 <- typeCheck te ty a
					if(b1) then return true else false
				_ -> false
		SecondTerm m -> do
			case m of
				TupleTerm a b -> do
					b1 <- typeCheck te ty b
					if(b1) then return true else false
				_ -> false
		WhenTerm l -> do
			b1 <- bThenThing te ty l 
			if(b1) then return true else false
		DerivativeTerm m tvs ts1 ts2 -> do
				b1 <- typeCheck (addTypedVarsToEnv tvs te) ty m
				tys <- map (\x -> getType  x) tvs
				z1 <- zip tys ts1
				lb2 <- map (\x -> typeCheck te (fst x) (snd x)) z1
				b2 <- and b2

				z2 <- zip tys ts2
				lb3 <- map (\x -> typeCheck te (fst x) (snd x)) z2
				b3 <- and b3
		
				if (b1&&b2&&b3) return true else return false
		 BlockTerm l m -> do
			c <- head te
			
			c' <- goThroughListOfStmts c  l

			case c' of
				Just c'' -> return typeCheck (c'':te) ty m

				Nothing -> return false
			
			
				
where cont = head te


"for every stmnt in l add the new typed vars to the context"
if the stmnt is just assignations, then it's easy
(should we check if a var is already in the cont? I don't think so...)
but what happnes when we hit a do?

maybe we need a function that takes a stmnt and returns the new context that arises from it
	it should also take the te


newContFinder:: -> context -> stmt -> maybe context
newContFinder cont s = 
	case s of
		SVar tv m -> if(typeCheck [cont] (getType tv) m) then just [(getName tv, getType tv):cont] else nothing

		SDo l b -> do
			c' <-  goThroughListOfStmts cont l
			b <- checkBool [c'] b
			if b then return just cont
			else return nothing


how should I go about it if things don't type with this?


do we even need to do anything for do stuff?
	we should still probably check if it types...


what do we want to do for the [stmt]?
-go through it 1 by 1
-if the current statement works, add to the context and keep going
-if it doesn't return nothing all around.


goThroughListOfStmts:: context -> [stmt] -> maybe context
goThroughListOfStmts cont [] = just cont
goThroughListOfStmts cont s:rest = do
	c <-(newContFinder cont s) 
	case c of
		Just c'-> return (goThroughListOfStmts c' rest)
		Nothing -> return nothing
-}

