module SDPLMonad where

import Lang.ErrM
import Control.Monad.Except 
import Control.Monad.Identity

import Data.Functor.Identity (Identity(runIdentity))
errMToException :: (Monad m ) => Err a -> ExceptT String m a
errMToException err = case err of 
    Left s -> throwError s 
    Right a -> return a 

type SDPLMonadT e m a = ExceptT e m a 
type SDPLMonad a = ExceptT String Identity a

runSDPLT :: (Monad m) => SDPLMonadT e m a -> m (Either e a)
runSDPLT = runExceptT

runSDPL :: SDPLMonad a -> Either String a 
runSDPL = runIdentity . runSDPLT