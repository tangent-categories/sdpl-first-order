module SimpleInterpreter where

import Lang.Abs
import qualified Data.Map as M
import Data.Text (Text)


weakHeadNormalizeWithEnv :: Term -> M.Map Text Term -> Term
weakHeadNormalizeWithEnv = const 