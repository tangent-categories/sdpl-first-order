module TermUtils where

import Lang.Abs
import qualified Data.Map as M
import Data.Text (Text)
import qualified Data.Text as T
import Data.Foldable


-- We use M.Map a () instead Data.Set.Set a so that we can intersect the free variables
-- with the function environment in the main loop.
type Set a = M.Map a ()

freeVarsFunction :: FunDef' a -> Set Text
freeVarsFunction (Function _ _ args _ body) = M.difference (freeVars body) argNames 
    where argNames = foldr (\(TypedVariable _ (PIdent (_, a_name)) _) -> M.insert a_name ()) M.empty args

getFunNameBody :: FunDef' a -> (PIdent,Term' a)
getFunNameBody (Function _ name _ _ body) = (name,body)

freeVars :: Term' a -> Set Text
freeVars t = freeVarsAccum t M.empty 

{-
Avoid creating singleton at all leaves by accumulating.
This also avoids performing expensive unions, though, this is somewhat offset as insertion is logn.

Invariant: freeVarsAccum t s = freeVars t \cup s
-}
freeVarsAccum :: Term' a -> Set Text -> Set Text
freeVarsAccum t accum = case t of
  Sub a x m n -> freeVarsAccum m $ M.delete (tyVarToText x) (freeVarsAccum n accum)
  Var a x -> M.insert (pIdentToText x) () accum
  RealTerm a num -> accum
  NullTerm a -> accum 
  AddTerms a m n -> freeVarsAccum m (freeVarsAccum n accum)
  UnitTerm a -> accum
  TupleTerm a te te' -> freeVarsAccum te (freeVarsAccum te' accum)
  FirstTerm a te -> freeVarsAccum te accum
  SecondTerm a te -> freeVarsAccum te accum
  WhenTerm a bts -> freeVarsAccumBTList bts accum
  DerivativeTerm _ m p pts dirs -> freeVarsAccumList dirs $ freeVarsAccumList pts $ M.difference (freeVarsAccum m accum) ps
    where ps = foldr (\xi -> M.insert (tyVarToText xi) ()) M.empty p
  BlockTerm a sts te -> 
      let (bound,free) = freeVarsAccumSTMList sts accum in M.difference (freeVarsAccum te free) bound
  MultFunctionTerm a te te' -> freeVarsAccum te $ freeVarsAccum te' accum
  SinFunctionTerm a te -> freeVarsAccum te accum
  CosFunctionTerm a te -> freeVarsAccum te accum

freeVarsAccumBT :: BThen' a -> Set Text -> Set Text
freeVarsAccumBT (BT _ b m) accum = freeVarsAccumBool b $ freeVarsAccum m accum

freeVarsAccumBTList :: Foldable t => t (BThen' a) -> Set Text -> Set Text
freeVarsAccumBTList bts accum = foldr freeVarsAccumBT accum bts

freeVarsAccumList :: Foldable t => t (Term' a) -> Set Text -> Set Text
freeVarsAccumList ts accum = foldr freeVarsAccum accum ts


freeVarsAccumSTMList :: Foldable t => t (Stmt' a) -> Set Text -> (Set Text,Set Text)
freeVarsAccumSTMList stmts accum = 
    foldr 
        (
            \stm (bound,free) -> 
                let (b_stm,f_stm) = freeVarsAccumSTM stm free
                in (M.union bound b_stm,f_stm)
        )
        (M.empty,accum)
        stmts


{-
Explanation: while accumulating the free variables in a list of statements:
   x1 = a1 
   x2 = a2
   ...
   xn = an

We first get the free variables of a1, and then we get the free variables of a2 minus x1, 
and then we get the free variables of a3, minus x1,x2, and so on and then finally, we bind this 
whole thing in m.  Alternatively, if one looks at the typing rules for do {stmts} in m
we have that Gamma |> stmts |> Gamma' and Gamma' |- m : B then Gamma |- do {stmts} in m : B
This means that all wee need to do is get the free variables in m, and subtract those created by 
Gamma.  However, m might not use all the free variables used by the statements, so we need 
get the free variables in the statements, and simultaneously build a set of formally block 
bound variables.
freeVarsAccumSTM hence returns two sets, the block bound variables, and the free-variables.
So that when binding in a term, we remove the bound variables.  Note that for a single statement, 
the block bound variables is at most a singleton.  (Draw picture to see why).
-}
freeVarsAccumSTM :: Stmt' a -> Set Text -> (Set Text,Set Text)
freeVarsAccumSTM stm accum = case stm of 
  SVar a x te -> (M.insert (tyVarToText x) () M.empty,freeVarsAccum te accum)
  SDo a sts b' -> 
      let (bound,free) = freeVarsAccumSTMList sts accum in (M.empty,M.difference bound (freeVarsAccumBool b' free))

freeVarsAccumBool :: B' a -> Set Text -> Set Text
freeVarsAccumBool b accum = case b of 
  LessThan a te te' -> freeVarsAccum te $ freeVarsAccum te' accum
  TrueBool a -> accum
  FalseBool a -> accum
  OrBool a p q -> freeVarsAccumBool p $ freeVarsAccumBool q accum
  AndBool a p q -> freeVarsAccumBool p $ freeVarsAccumBool q accum






pIdentToText :: PIdent -> Text 
pIdentToText (PIdent (_,name)) = name

tyVarToText :: Tyvar' a -> Text 
tyVarToText (TypedVariable _ x _) = pIdentToText x